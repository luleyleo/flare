<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2022 Julian Schmidhuber -->
<component type="desktop-application">
  <id>@app-id@</id>
  <project_license>AGPL-3.0</project_license>
  <metadata_license>FSFAP</metadata_license>
  <!-- Translators: Flare name in metainfo. -->
  <name>Flare</name>
  <!-- Translators: Flare summary in metainfo. -->
  <summary>Chat with your friends on Signal</summary>
  <launchable type="desktop-id">@app-id@.desktop</launchable>
  <content_rating type="oars-1.1"/>
  <developer_name translatable="no">Julian Schmidhuber</developer_name>
  <developer id="de.schmidhuberj.mobile">
    <name>Julian Schmidhuber</name>
  </developer>
  <translation type="gettext">@gettext-package@</translation>
  <update_contact>flatpak@schmidhuberj.de</update_contact>

  <url type="homepage">https://mobile.schmidhuberj.de/flare</url>
  <url type="bugtracker">https://gitlab.com/schmiddi-on-mobile/flare/-/issues</url>
  <url type="translate">https://hosted.weblate.org/engage/schmiddi-on-mobile/</url>
  <url type="contact">https://matrix.to/#/#flare-signal:matrix.org</url>
  <url type="vcs-browser">https://gitlab.com/schmiddi-on-mobile/flare</url>
  <url type="contribute">https://gitlab.com/schmiddi-on-mobile/flare/-/blob/master/CONTRIBUTING.md</url>

  <description>
    <!-- Translators: Description of Flare in metainfo. -->
    <p>
      Flare is an unofficial app for Signal. It is still in development and doesn't include all the features that the official Signal apps do. More information can be found on its feature roadmap.
    </p>
    <!-- Translators: Description of Flare in metainfo: Security note -->
    <p>
      Please note that using this application will probably worsen your security compared to using official Signal applications.
      Use with care when handling sensitive data.
      Look at the projects README for more information about security.
    </p>
  </description>

  <releases>
    <release version="v0.14.2" date="2024-04-02">
      <description translatable="no">
        <p>Added</p>
        <ul>
          <li>Withdraw notification when message has been read.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Failing to load groups and messages when started offline.</li>
          <li>Improve performance on startup.</li>
          <li>Sometimes the avatar and name showing for own messages.</li>
        </ul>
        <p>Chores</p>
        <ul>
          <li>Cleanup major parts of the code.</li>
          <li>Add developer documentation.</li>
        </ul>
      </description>
    </release>
    <release version="v0.14.1" date="2024-03-22">
      <description translatable="no">
        <p>Added</p>
        <ul>
          <li>Revamped channel info dialog.</li>
        </ul>
      </description>
    </release>
    <release version="v0.14.0" date="2024-03-20">
      <description translatable="no">
        <p>HOTFIX</p>
        <ul>
          <li>Groups not working for newly linked devices.</li>
        </ul>
        <p>Added</p>
        <ul>
          <li>Dialog for adding a new channel.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>UUID not found issues for some groups.</li>
        </ul>
        <p>Chores</p>
        <ul>
          <li>Updated GTK and libadwaita dependencies to GNOME 46 versions.</li>
        </ul>
      </description>
    </release>
    <release version="v0.13.0" date="2024-02-25">
      <description translatable="no">
        <p>Added</p>
        <ul>
          <li>Avatars for contacts and groups.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Persistent typing messages</li>
          <li>Possible crashes while linking</li>
        </ul>
      </description>
    </release>
    <release version="v0.12.0" date="2024-02-11">
      <description translatable="no">
        <p>Added</p>
        <ul>
          <li>Draft message for each chat.</li>
          <li>Display of group descriptions.</li>
          <li>Reception of typing indicators.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>The sidebar now highlights the active chat.</li>
          <li>Grab focus of the entry when replying to a message.</li>
          <li>UI improvements to the channel information dialog, which now also displays phone number, disappearing messages timer and description.</li>
          <li>Fix the messages being sent on shift+enter instead of ctrl+enter.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Random color of note-to-self chat.</li>
        </ul>
      </description>
    </release>
    <release version="v0.11.2" date="2024-01-04">
      <description translatable="no">
        <p>HOTFIX</p>
        <ul>
          <li>Backend panic after suspend.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Use Control instead of Shift for invertet action on the text input.</li>
        </ul>
      </description>
    </release>
    <release version="v0.11.1" date="2023-12-30">
      <description translatable="no">
        <p>Fixed</p>
        <ul>
          <li>High memory usage due to having unused emoji pickers.</li>
          <li>High memory usage of the setup window, even if unused.</li>
          <li>Send on Ctrl+Enter when sending on Enter is disabled.</li>
          <li>Long starting times due to formatting of contacts.</li>
          <li>Quitting the application turning off running in the background.</li>
        </ul>
      </description>
    </release>
    <release version="v0.11.0" date="2023-12-25">
      <description translatable="no">
        <p>HOTFIX</p>
        <ul>
          <li>Contacts syncing.</li>
          <li>Linking device.</li>
        </ul>
        <p>Added</p>
        <ul>
          <li>A date-divider between messages sent on different days.</li>
          <li>Initial work for usage of Flare as a primary device (not yet enabled by default).</li>
          <li>Menu entry for contact sync.</li>
          <li>Menu entry for stopping Flare, even if it is running in the background.</li>
          <li>An improved setup window, which introduces Flare, gives the option to either link or use Flare as primary device (not yet enabled by default), insert required information and gives further information about Flare, e.g. some known issues or contact information.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>Better UI for showing the last message of a channel.</li>
          <li>Only show the message timestamp if it differs from the next message.</li>
          <li>Better UI for the message entry bar.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Unlinking Flare while running in the background is enabled not bringing up the relink window at next start.</li>
        </ul>
        <p>Chores</p>
        <ul>
          <li>Updated to GTK 4.12 and Libadwaita 1.4 and all other dependencies.</li>
        </ul>
      </description>
    </release>
    <release version="v0.10.0" date="2023-08-28">
      <description translatable="no">
        <p>Added</p>
        <ul>
          <li>Deleting messages of a channel locally.</li>
          <li>Blurred placeholders for attachments.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>Better UI for linking.</li>
          <li>Better UI for attachments.</li>
          <li>Better UI for message entry.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Handle libsecret stores that can lock specific items.</li>
          <li>Restrict sending of a file-attachment such that a file-attachment must be sent without any additional attachments (restriction from Signal).</li>
        </ul>
      </description>
    </release>
    <release version="v0.9.3" date="2023-08-17">
      <description translatable="no">
        <p>Added</p>
        <ul>
          <li>Support for libspelling.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Fixed performance issues when loading messages.</li>
        </ul>
      </description>
    </release>
    <release version="v0.9.2" date="2023-08-09">
      <description translatable="no">
        <p>HOTFIX</p>
        <ul>
          <li>Fix linking being broken.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>Updated description and namespace.</li>
        </ul>
      </description>
    </release>
    <release version="v0.9.1" date="2023-08-02">
      <description translatable="no">
        <p>Added</p>
        <ul>
          <li>Notifications on reactions received (configurable).</li>
          <li>Setting to turn off sending a message when the enter-key is pressed.</li>
          <li>Ability to submit captcha requests.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>Text of the quote in the reply message is now truncated to 2 lines.</li>
          <li>Moved the download and open attachments buttons to the popover.</li>
          <li>Don't hard-code the font size for texts.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>General UI fixes.</li>
        </ul>
        <p>Chores</p>
        <ul>
          <li>Update all dependencies. This especially includes presage/libsignal-service-rs backends becoming post-quantum secure.</li>
        </ul>
      </description>
    </release>
    <release version="v0.9.0" date="2023-07-10">
      <description translatable="no">
        <p>Changed</p>
        <ul>
          <li>Added support for voice messages/audio files.</li>
          <li>Ported the ui to Blueprint.</li>
          <li>New UI improvements to the feed list. This also now respects the accent colors set in GNOME.</li>
          <li>Reworked the popover menu for messages.</li>
          <li>Updated to GTK 4.10 with many UI improvements and minor fixes.</li>
          <li>Message popups are now opened with right-click or long-press (touch only).</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Issue where special characters in replied messages were displayed incorrectly.</li>
          <li>Attachments with very long names not being ellipsized.</li>
          <li>Messages with only attachment not persisting reactions.</li>
        </ul>
      </description>
    </release>
    <release version="v0.8.2" date="2023-05-29">
      <description translatable="no">
        <p>Added</p>
        <ul>
          <li>A &quot;pick emoji&quot; button for the text entry.</li>
          <li>Support for receiving mentions.</li>
          <li>Smaller UI improvements.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Avatar of contacts with special characters not displaying the initials label.</li>
          <li>No profile names were shown in notifications.</li>
          <li>Removing expiration timer for conversations with expire messages.</li>
        </ul>
      </description>
    </release>
    <release version="v0.8.1" date="2023-05-07">
      <description translatable="no">
        <p>Added</p>
        <ul>
          <li>Button to show all channels.</li>
          <li>Button to scroll down in the messages view.</li>
          <li>Keyboard shortcut Ctrl+Q to close window.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>Only show non-empty channels by default.</li>
          <li>UI changes to differentiate own messages from others.</li>
        </ul>
      </description>
    </release>
    <release version="v0.8.0" date="2023-05-03">
      <description translatable="no">
        <p>Changed</p>
        <ul>
          <li>Rewrote the message list to be a ListView.</li>
          <li>Slight fix for the icon.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Switches in the settings having inconsistent activation.</li>
          <li>Possibly fixed session corruptions upstream.</li>
          <li>Don't ignore emoji remove reactions.</li>
          <li>Fixed sending wrong reaction with complex emojis.</li>
          <li>Fixed groups not working (upstream).</li>
        </ul>
      </description>
    </release>
    <release version="v0.7.2" date="2023-04-08">
      <description translatable="no">
        <p>Added</p>
        <ul>
          <li>Message deletion.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>Updated application icon and emblem.</li>
          <li>Don't receive messages from blocked contacts anymore.</li>
        </ul>
      </description>
    </release>
    <release version="v0.7.1" date="2023-04-01">
      <description translatable="no">
        <p>HOTFIX</p>
        <ul>
          <li>Fix outgoing 1-to-1 messages stored incorrectly.</li>
        </ul>
      </description>
    </release>
    <release version="v0.7.0" date="2023-03-29">
      <description translatable="no">
        <p>BREAKING</p>
        <ul>
          <li>Due to a new storage for data, a relink is required on first startup</li>
        </ul>
        <p>Added</p>
        <ul>
          <li>Integration with feedbackd.</li>
          <li>Showing profile names.</li>
          <li>Copy action in message menu.</li>
          <li>Initial channel information dialog with identity reset.</li>
          <li>Show offline status in GUI.</li>
        </ul>
        <p>Changed</p>
        <ul>
          <li>Notifications for group messages now display the name of the group instead of just the name of the sender.</li>
          <li>Messages must now be successfully sent before displaying in a chat.</li>
          <li>UI got revamped.</li>
        </ul>
        <p>Fixed</p>
        <ul>
          <li>Fixed not setting revision for groups.</li>
          <li>Prevent sending messages while offline.</li>
        </ul>
        <p>Chores</p>
        <ul>
          <li>Updated all dependencies.</li>
        </ul>
      </description>
    </release>
    <release version="v0.6.0" date="2023-01-13">
      <description translatable="no">
        <p>Added</p>
        <ul>
        <li>Ability to receive messages in the background.</li>
        </ul>
        <p>Fixed</p>
        <ul>
        <li>Fixed restarting the backend after suspend.</li>
        <li>Fixed grammar error in the project description.</li>
        </ul>
      </description>
    </release>
    <release version="v0.5.7" date="2022-12-17">
      <description translatable="no">
        <p>Fixed</p>
        <ul>
        <li>Fixed application freeze when there are many initial messages</li>
        <li>Fixed incorrect ordering of new messages</li>
        </ul>
      </description>
    </release>
    <release version="v0.5.6" date="2022-12-03">
      <description translatable="no">
        <p>Fixed</p>
        <ul>
        <li>Fixed memory leak.</li>
        <li>Fixed displayed reply message not clearing attachments when changing reply.</li>
        <li>Fixed crash when searching non-existing channel.</li>
        </ul>
      </description>
    </release>
    <release version="v0.5.5" date="2022-11-16">
      <description translatable="no">
        <p>HOTFIX</p>
        <ul>
        <li>Fix linking not working</li>
        </ul>
      </description>
    </release>
    <release version="v0.5.4" date="2022-11-15">
      <description translatable="no">
        <p>Changed</p>
        <ul>
        <li>Slight UI update to channel list</li>
        </ul>
        <p>Fixed</p>
        <ul>
        <li>Crash when clicking on call message</li>
        <li>URLs containing &quot;&amp;&quot; not being displayed</li>
        <li>Fixed some (hopefully all) duplicate messages</li>
        <li>Fixed receiving messages from certain groups</li>
        </ul>
      </description>
    </release>
    <release version="v0.5.3" date="2022-10-27">
      <description translatable="no">
        <p>HOTFIX:</p>
        <ul>
        <li>Updated certificate of Signal servers</li>
        </ul>
      </description>
    </release>
    <release version="v0.5.2" date="2022-10-16">
      <description translatable="no">
        <p>Added:</p>
        <ul>
          <li>Pasting files and images into the text entry.</li>
          <li>Unlink without deleting messages.</li>
          <li>Messages and notifications for calls.</li>
        </ul>
        <p>Fixed:</p>
        <ul>
          <li>Fixed showing messages with &quot;&amp;&quot;, &quot;&lt;&quot; or &quot;&gt;&quot;.</li>
        </ul>
        <p>Chores:</p>
        <ul>
          <li>Refactored GTK properties.</li>
          <li>Greatly refactored messages.</li>
        </ul>
      </description>
    </release>
    <release version="v0.5.1" date="2022-10-10">
      <description translatable="no">
        <p>Added:</p>
        <ul>
          <li>Clickable links in text messages.</li>
          <li>Open image in default program.</li>
          <li>Contributing guidelines</li>
        </ul>
        <p>Changed:</p>
        <ul>
          <li>Show backend thread panics.</li>
          <li>UI improvements for the channel list.</li>
          <li>Display user initials in profile pictures.</li>
        </ul>
        <p>Fixed:</p>
        <ul>
          <li>Fixed duplicate message receiving.</li>
          <li>Wrapping for extremely long words.</li>
        </ul>
      </description>
    </release>
    <release version="v0.5.0" date="2022-10-05">
      <description translatable="no">
        <p>Added:</p>
        <ul>
          <li>Immediately sync contacts after linking, without application restart required.</li>
          <li>Show UUID for unknown contacts (without name, phone numbers) instead of empty string.</li>
          <li>Notification support.</li>
        </ul>
        <p>Changed:</p>
        <ul>
          <li>Many UI updates.</li>
          <li>Use new libadwaita widgets (about dialog, message dialog, entry row).</li>
          <li>New message storage backend.</li>
        </ul>
        <p>Fixed:</p>
        <ul>
          <li>Wrongly associated stored messages from other devices.</li>
          <li>Possibly fixed libsecret-related issue.</li>
          <li>(Upstream) Fixed duplicate messages to other third-party Signal clients.</li>
        </ul>
        <p>BREAKING:</p>
        <ul>
          <li>Due to changes in the storage backend, your previously stored messages will be lost.</li>
        </ul>
      </description>
    </release>
    <release version="v0.4.1" date="2022-09-20">
      <description translatable="no">
        <p>Added:</p>
        <ul>
          <li>Button in the link window to copy link URL to clipboard.</li>
          <li>Group storage.</li>
          <li>Search for the channel view.</li>
        </ul>
        <p>Fixed:</p>
        <ul>
          <li>Maybe fixed rare crash in the backend thread.</li>
          <li>Fixed messages sent to contacts being wrongly stored to &quot;Note to self&quot;.</li>
        </ul>
      </description>
    </release>
    <release version="v0.4.0" date="2022-09-16">
      <description translatable="no">
        <p>Added:</p>
        <ul>
          <li>Accessibility (no idea how good it is).</li>
          <li>Message storage.</li>
          <li>Prevented sending empty messages.</li>
          <li>Display the time the message was sent.</li>
        </ul>
        <p>Fixed:</p>
        <ul>
          <li>Attachment list not hiding after sending message.</li>
          <li>Minor glib warning.</li>
        </ul>
      </description>
    </release>
    <release version="v0.3.3" date="2022-09-05">
      <description translatable="no">
        <p>Added:</p>
        <ul>
          <li>Keyboard shortcuts.</li>
        </ul>
        <p>Changed:</p>
        <ul>
          <li>Small UI updates.</li>
          <li>Moved from Entry to TextView which provides multi-line editing and line wrapping.</li>
        </ul>
        <p>Fixed:</p>
        <ul>
          <li>Fixed screenshot dummy.</li>
        </ul>
      </description>
    </release>
    <release version="v0.3.2" date="2022-08-30">
      <description translatable="no">
        <p>Hotfix for crash without libsecret set up.</p>
      </description>
    </release>
    <release version="v0.3.1" date="2022-08-25">
      <description translatable="no">
        <p>Fix gschema not being installed.</p>
      </description>
    </release>
    <release version="v0.3.0" date="2022-08-25">
      <description translatable="no">
        <p>Implemented device name for linking, added support for video attachment, configurable lazy-load attachments, bug fixes.</p>
      </description>
    </release>
    <release version="v0.2.1" date="2022-08-15">
      <description translatable="no">
        <p>Hotfix for not beeing able to link and unlink the device.</p>
      </description>
    </release>
    <release version="v0.2.0" date="2022-08-07">
      <description translatable="no">
        <p>UI updates, more translations, bug fixing.</p>
      </description>
    </release>
    <release version="v0.1.5" date="2022-07-23">
      <description translatable="no">
        <p>Added ability to upload and download attachments, added ability to remove reply and attachments before sending, fixed some bugs mainly regarding reply messages.</p>
      </description>
    </release>
    <release version="v0.1.4" date="2022-07-16">
      <description translatable="no">
        <p>Fixed many bugs, including wrong association for messages to channels, wrongly displayed channel names as "Note to self" and a few crashes.</p>
      </description>
    </release>
    <release version="v0.1.3" date="2022-07-10">
      <description translatable="no">
        <p>Fix message receiving, sending attachments.</p>
      </description>
    </release>
    <release version="v0.1.2" date="2022-07-08">
      <description translatable="no">
        <p>Bug fixing, error handling, russian and german translation, small UI updates.</p>
      </description>
    </release>
    <release version="v0.1.1" date="2022-07-05">
      <description translatable="no">
        <p>Small UI changes, change of license.</p>
      </description>
    </release>
    <release version="v0.1.0" date="2022-06-20">
      <description translatable="no">
        <p>Initial Release.</p>
      </description>
    </release>
  </releases>

  <screenshots>
    <screenshot type="default">
      <caption>Overview of the application</caption>
      <image>https://gitlab.com/Schmiddiii/flare/-/raw/master/data/screenshots/screenshot.png</image>
    </screenshot>
  </screenshots>

  <provides>
    <binary>flare</binary>
  </provides>


  <requires>
    <internet>first-run</internet>
    <display_length compare="ge" side="shortest">300</display_length>
    <display_length compare="ge" side="longest">360</display_length>
  </requires>
  <recommends>
    <internet>always</internet>
  </recommends>
  <supports>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </supports>
</component>

