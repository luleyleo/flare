using Gtk 4.0;
using Adw 1;

template $FlWindow: Adw.ApplicationWindow {
  can-focus: true;
  title: "Flare";
  height-request: 294;
  width-request: 360;

  Adw.Breakpoint {
    condition ("max-width: 720sp")

    setters {
      split_view.collapsed: true;
    }
  }

  Adw.NavigationSplitView split_view {
    min-sidebar-width: 300;
    max-sidebar-width: 400;
    sidebar-width-fraction: 0.25;
    notify::show-content => $handle_show_content() swapped;

    sidebar: Adw.NavigationPage {
      title: _("Channel list");
      tag: "channel-list";

      Adw.ToolbarView {
        [top]
        Adw.HeaderBar {
          title-widget: Adw.WindowTitle {
            title: "Flare";
          };

          [start]
          Gtk.Button {
            accessibility {
              label: C_("accessibility", "Add Conversation");
            }
            
            tooltip-text: "Add Conversation";
            icon-name: "chat-message-new-symbolic";
            clicked => $handle_add_conversation_clicked() swapped;
          }

          [end]
          MenuButton {
            accessibility {
              label: C_("accessibility", "Menu");
            }

            tooltip-text: "Menu";
            menu-model: menubar;
            primary: true;
            icon-name: "open-menu-symbolic";
          }

          [end]
          ToggleButton {
            accessibility {
              label: C_("accessibility", "Search");
            }

            tooltip-text: "Search";
            icon-name: "system-search-symbolic";
            active: bind channel_list.search-enabled no-sync-create;
            clicked => $handle_search_clicked() swapped;
          }
        }

        content: $FlChannelList channel_list {
          notify::active-channel => $handle_go_forward() swapped;
          manager: bind template.manager;
        };
      }
    };

    content: Adw.NavigationPage {
      title: _("Chat");
      tag: "chat";

      Adw.ToolbarView {
        [top]
        Adw.HeaderBar {
          title-widget: Button {
            styles ["flat"]
            sensitive: bind $is_some(channel_list.active-channel) as <bool>;
            visible: bind $is_some(channel_list.active-channel) as <bool>;
            action-name: "win.channel-information";

            child: Box {
              name: "room_title";
              orientation: vertical;
              halign: center;
              valign: center;

              Label title_label {
                focusable: true;
                ellipsize: end;
                halign: center;
                wrap: false;
                single-line-mode: true;
                use-markup: true;
                width-chars: 5;
                label: bind channel_list.active-channel as <$FlChannel>.title;
                styles [
                  "title",
                ]
              }

              Label subtitle_label {
                focusable: true;
                ellipsize: end;
                halign: center;
                wrap: false;
                single-line-mode: true;
                use-markup: true;
                visible: bind channel_list.active-channel as <$FlChannel>.is-typing;
                label: _("is typing");
                tooltip-markup: bind channel_list.active-channel as <$FlChannel>.description;

                styles [
                  "subtitle-room",
                  "accent" 
                ]
              }
            };
          };
        }

        content: $FlChannelMessages channel_messages {
          manager: bind template.manager;
          active-channel: bind channel_list.active-channel;
        };
      }
    };
  }
}

$FlNewChannelDialog new_channel_dialog {
  window: template;

  channel => $handle_new_channel() swapped;
}


menu menubar {
  section {
    item {
      label: _("Settings");
      action: "win.settings";
    }

    item {
      label: _("Unlink");
      action: "win.unlink";
    }

    item {
      label: _("Linked Devices");
      action: "win-managed.linked-devices";
      hidden-when: "action-disabled";
    }

    item {
      label: _("Clear messages");
      action: "win.clear-messages";
    }

    item {
      label: _("Submit Captcha");
      action: "win.submit-captcha";
    }

    item {
      label: _("Synchronize Contacts");
      action: "win.sync-contacts";
    }

    item {
      label: _("Keybindings");
      action: "win.show-help-overlay";
    }

    item {
      label: _("About");
      action: "win.about";
    }

    item {
      label: _("Quit");
      action: "win.kill";
    }
  }
}
